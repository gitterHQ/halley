'use strict';

var assert = require('assert');
var Promise = require('bluebird');

var OUTAGE_TIME = 5000;

module.exports = function() {
  describe('client-bad-connection', function() {

    it('should deal with dropped packets', function() {
      var count = 0;
      var postOutageCount = 0;
      var outageTime;
      var outageGraceTime;
      var self = this;

      var checkOutageFaultTolerance = new Promise(function(resolve, reject) {
        self.client.subscribe('/datetime', function() {
          count++;

          // After one message, create an short outage
          if (count === 1) {
            return self.serverControl.networkOutage(OUTAGE_TIME)
              .then(function() {
                outageTime = Date.now();
                outageGraceTime = Date.now() + 1000;
              })
              .catch(function(err) {
                reject(err);
              });
          }

          if (!outageTime) return;
          if (outageGraceTime >= Date.now()) return;

          postOutageCount++;

          // Check to see we have 3 successful messages after the outage
          if (postOutageCount >= 3) {
            assert(Date.now() - outageTime >= (OUTAGE_TIME * 0.8));
            resolve();
          }
        });
      });

      return checkOutageFaultTolerance;
    });

    it('should emit connection events', function() {
      var client = this.client;

      var checkUp1 = new Promise(function(resolve) {
        client.once('connection:up', function() {
          resolve();
        });
      });

      return client.connect()
        .bind(this)
        .then(function() {
          // Subscribe to messages
          var checkGotMessage = new Promise(function(resolve) {
            client.subscribe('/datetime', function() {
              resolve();
            });
          });

          // Awaiting initial connection:up
          return Promise.all([
            checkUp1,
            checkGotMessage
          ]);
        })
        .then(function() {
          var checkDown2 = new Promise(function(resolve) {
            client.once('connection:down', function() {
              resolve();
            });
          });

          // Wait for the server to go down and notify us
          return Promise.all([
            this.serverControl.networkOutage(),
            checkDown2
          ]);
        })
        .then(function() {
          var checkUp3 = new Promise(function(resolve) {
            client.once('connection:up', function() {
              resolve();
            });
          });

          return Promise.all([
            this.serverControl.restoreAll(),
            checkUp3
          ]);
        });
    });

    it('should emit connection events without messages', function() {
      var client = this.client;

      var checkUp1 = new Promise(function(resolve) {
        client.once('connection:up', function() {
          resolve();
        });
      });

      return client.connect()
        .bind(this)
        .then(function() {
          // Awaiting initial connection:up
          return checkUp1;
        })
        .then(function() {
          var checkDown2 = new Promise(function(resolve) {
            client.once('connection:down', function() {
              resolve();
            });
          });

          // Wait for the server to go down and notify us
          return Promise.all([
            this.serverControl.networkOutage(),
            checkDown2
          ]);
        })
        .then(function() {
          var checkUp3 = new Promise(function(resolve) {
            client.once('connection:up', function() {
              resolve();
            });
          });

          return Promise.all([
            this.serverControl.restoreAll(),
            checkUp3
          ]);
        });
    });

  });

};
